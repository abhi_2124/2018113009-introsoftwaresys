from matrix import Matrix

class Vector(Matrix):
    def transpose(self):
        print("error : transpose not defined for vector type")

    def __getitem__(self, index):
        return self.mat[index][0]

    def __setitem__(self, index, value):
        self.mat[index][0] = value

    def __mod__(self, other):
        if self.check(other):
            print("error : incompatible vectors")
        else:
            temp = 0
            for i in self.rows:
                temp += self[i] * other[i]
            return temp

    def norm(self):
        temp=0
        for i in self.rows:
            temp += self[i]**2
        temp = temp**0.5
