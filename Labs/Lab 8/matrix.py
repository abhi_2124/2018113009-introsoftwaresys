class Matrix:
    def __init__(self, *args):
        self.rows = args[0]
        self.cols = args[1]
        self.mat = [[0] * self.cols for i in range(self.rows) ] 
        if len(args) != 2:
            self.mat=args[2]

    def __repr__(self):
        temp = [None] * self.rows
        for i in range(self.rows):
            temp[i] = ' '.join(map(str,self.mat[i]))
        return '\n'.join(map(str, temp))
    
    def __getitem__(self, tupple):
        x, y = tupple
        return self.mat[x][y]

    def __setitem__(self, tupple, value):
        x, y = tupple
        self.mat[x][y] = value
    
    def check(self, other):
        if other.rows!=self.rows | other.cols!=self.cols:
            return 1
        else:
            return 0

    def __add__(self, other):
        if self.check(other):
            print("error : matrix mismatch")
        else:
            temp=Matrix()
            for i in range(self.rows):
                for j in range(self.cols):
                    temp[i,j] = self[i,j] + other[i,j]
            return temp
    
    def __sub__(self, other):
        if self.check(other):
            print("error : matrix mismatch")
        else:
            temp=Matrix()
            for i in range(self.rows):
                for j in range(self.cols):
                    temp[i,j] = self[i,j] - other[i,j]
            return temp

    def __mul__(self, other):
        if self.cols != other.rows:
            print("error : matrix mismatch")
        else:
            temp=Matrix()
            for i in range(self.rows):
                for j in range(self.cols):
                    temp[i,j] = 0
                    for k in range(self.cols):
                        temp[i,j] += self[i,k] * other[k,j]
            return temp

    def __pow__(self, other):
        if self.check(other):
            print("error : matrix mismatch")
        else:
            temp=Matrix()
            for i in range(self.rows):
                for j in range(self.cols):
                    temp[i,j] = self[i,j] * other[i,j]
            return temp

    def __floordiv__(self, other):
        if self.check(other):
            print("error : matrix mismatch")
        else:
            temp=Matrix()
            for i in range(self.rows):
                for j in range(self.cols):
                    if other[i,j] == 0:
                        print("error : divison by zero")
                        exit
                    temp[i,j] = self[i,j] / other[i,j]
            return temp

    def transpose(self):
        temp = Matrix(self.cols, self.rows)
        for i in range(self.cols):
            for j in range(self.rows):
                temp.mat[i][j] = self.mat[j][i]
        return temp

m1 = Matrix(2,3,[ [1,2,3], [4,5,6] ])
print(m1)
print(m1.transpose())
m1[1,1] = 0
print(m1[1,1])

